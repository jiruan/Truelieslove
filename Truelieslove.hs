module Truelieslove where
-- Written by Ji Ruan for a contribution to Jan van Eijck
-- May 2017
-- Import modules from Jan's DEMO Light (slightly modified version)

import Data.List
import ModelsVocab hiding (m0)
import ActionVocab
import ChangeVocab hiding (public, preconditions, voc)
import ChangePerception

h, heleen, j,jan :: Agent
h = Ag 5; heleen = Ag 5
j = Ag 6; jan = Ag 6

rh, rj :: Form
rh = Prp (RH) -- this represents Heleen comes to the retreat
rj = Prp (RJ) -- this represents Jan comes to the retreat

mod0 :: EpistM Integer
mod0 = (Mo [0..3] [h,j] [RH, RJ] val accs points)
  where
   val = [(0,[]),(1,[RJ]),(2,[RH]),(3,[RH,RJ])]
   accs = [(h,x,y) | x <- [0,1], y <- [0,1]] ++ [(h,x,y) | x <- [2,3], y <- [2,3]] ++ [(j,x,y) | x <- [0,2], y <- [0,2]] ++ [(j,x,y) | x <- [1,3], y <- [1,3]]
   points = [0]

test0a = isTrue mod0 (K h (Neg rh))
test0b = isTrue mod0 (K h (Neg rj))
test0c = isTrue mod0 (K j (Neg rj))
test0d = isTrue mod0 (K j (Neg rh))

--FAM is a function from ags to action models
amlie2h :: FAM State --Private lie to Heleen
amlie2h ags = Am [0,1,2] ags [(0,(Neg rj)), (1,rj), (2, Top)]
             [(h,0,1), (h,1,1), (h,2,2), (j,0,2), (j,1,2), (j,2,2)] [0]

amlie2j :: FAM State --Private lie to Jan
amlie2j ags = Am [0,1,2] ags [(0,(Neg rh)), (1,rh), (2, Top)]
             [(j,0,1), (j,1,1), (j,2,2), (h,0,2), (h,1,2), (h,2,2)] [0]

mod1 = up mod0 amlie2h -- result of lying to Heleen
mod1' = bisim mod1 -- generated submodel of mod1 under bisim
mod2 = up mod1' amlie2j -- result of lying to Jan
mod2' = bisim mod2 -- generated submodel of mod2 under bisim

januncertain = Neg (Disj [(K j rh), (K j (Neg rh))])
heleenbelief = Conj [Neg rh, K h rj, K h januncertain]

heleenuncertain = Neg (Disj[(K h rj), (K h (Neg rj))])
janbelief = Conj [Neg rj, K j rh, K j heleenuncertain]

test1h = isTrue mod2 heleenbelief
test1j = isTrue mod2 janbelief

acm4h :: FACM State -- Heleen changes her mind
acm4h ags = Acm [0,1] ags [(0,(Top,[(RH, K h rj)])), (1,(Top,[]))]
                [ (h,0,0),(h,1,1), (j,0,1), (j,1,1) ] [0]
acm4j :: FACM State -- Jan changes his mind
acm4j ags = Acm [0,1] ags [(0,(Top,[(RJ, K h rh)])), (1,(Top,[]))]
                [ (j,0,0),(j,1,1), (h,0,1), (h,1,1) ] [0]

mod3 = upc mod2' acm4h -- result of Heleen changing her mind
mod3' = bisim mod3
mod4 = upc mod3' acm4j -- result of Jan changing his mind
mod4' = bisim mod4

heleenbeliefnew = K h (K j (heleenuncertain))
janbeliefnew = K j (K h (januncertain))

test2h = isTrue mod4 heleenbeliefnew
test2j = isTrue mod4 janbeliefnew

mod5 = upc mod1' acm4h -- Heleen changed her mind right after being lied.
mod5' = bisim mod5
mod6 = upc mod5' acm4j -- mod6 Jan changed his mind right after Heleen's change.
mod6' = bisim mod6

test3h = isTrue mod6 heleenbeliefnew
test3j = isTrue mod6 janbeliefnew
