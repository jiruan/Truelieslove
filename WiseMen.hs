module WiseMen

where 
import Data.List
import ModelsVocab hiding (m0)
import ActionVocab hiding (upd,public,preconditions,voc)
import ChangeVocab
import ChangePerception

mo0 = initM [a,b,c,d] [P 1, P 2, P 3, P 4]

p1,p2,p3,p4 :: Form
p1 = Prp (P 1); p2 = Prp (P 2)
p3 = Prp (P 3); p4 = Prp (P 4)

capsInfo :: Form
capsInfo = 
  Disj [Conj [f, g, Neg h, Neg j] | 
              f <- [p1, p2, p3, p4], 
              g <- [p1, p2, p3, p4] \\ [f],
              h <- [p1, p2, p3, p4] \\ [f,g],
              j <- [p1, p2, p3, p4] \\ [f,g,h], 
              f < g, h < j                     ]

mo1 = upd mo0 (public capsInfo)

awarenessFirstCap  = info [b,c] p1
awarenessSecondCap = info [c]   p2

mo2  = upd (upd mo1 awarenessFirstCap)
                    awarenessSecondCap 

bK =  Disj [K b p2, K b (Neg p2)]
cK =  Disj [K c p3, K c (Neg p3)]

mo3a = upd  mo2 (public cK) 
mo3b = upd  mo2 (public (Neg cK))

impl :: Form -> Form -> Form 
impl form1 form2 = Disj [Neg form1, form2]

equiv :: Form -> Form -> Form 
equiv form1 form2 = 
  Conj [form1 `impl` form2, form2 `impl` form1]

test1 = isTrue mo3a bK
test2 = isTrue mo3b bK
test3 = isTrue mo3a (K a (equiv p1 p2))
test4 = isTrue mo3b (K a (equiv p1 (Neg p2)))

mo4a = upd mo3a (public bK)
mo4b = upd mo3b (public bK)

