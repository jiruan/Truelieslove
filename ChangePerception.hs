module ChangePerception where

import Data.List
import ModelsVocab hiding (m0)
import ActionVocab hiding (upd,public,preconditions,voc)
import ChangeVocab

unobserved_change :: Prp -> Form -> FACM State
unobserved_change prp form ags =
  Acm
   [0,1]
   ags
   [(0,(Top,[(prp,form)])), (1,(Top,[]))]
   [(a,s,t)| a <- ags, s <- [0,1], t <- [0,1]]
   [0]

perception :: Agent -> Form -> [Agent] -> FACM State
perception i form group ags =
  Acm [0,1,2]
       ags
      [(0,(form,[])),(1,(Neg form, [])), (2,(Top,[]))]
      ([(a,s,s)| a <- ags, s <- [0,1,2]]
        ++ [(a,0,1)| a <- ags \\ [i]]
        ++ [(a,1,0)| a <- ags \\ [i]]
        ++ [(a,0,2)| a <- ags \\ group]
        ++ [(a,2,0)| a <- ags \\ group]
        ++ [(a,1,2)| a <- ags \\ group]
        ++ [(a,2,1)| a <- ags \\ group])
      [0]

md0 :: EpistM Integer
md0 = Mo [0,1]
         [a,b,c]
         [P 0]
         [(0,[P 0]), (1,[])]
         ([(ag,x,x) | ag <- [a,b,c], x <- [0,1]]
               ++ [(c,0,1),(c,1,0)])
         [0]

md1 = updc md0 (perception b (Prp (P 0)) [b,c])
md1' = upc md0 (perception b (Prp (P 0)) [b,c])

md2 = updc md1 (unobserved_change (P 0) (Neg Top))
md2' = upc md1 (unobserved_change (P 0) (Neg Top))

md3 = updc md2 (perception b (Neg p) [b,c])
md3' = upc md2 (perception b (Neg p) [b,c])

md3a = updc md2 (perception b (Neg p) [a,b,c])
md3a' = upc md2 (perception b (Neg p) [a,b,c])

md1a = updc md0 (perception c p [a,c])
md1a' = upc md0 (perception c p [a,c])

mm0 :: EpistM Integer
mm0 = Mo [0,1]
         [a,b,c]
         [P 0]
         [(0,[P 0]), (1,[])]
         ([(ag,x,x) | ag <- [a,b,c], x <- [0,1]]
            ++ [(a,0,1),(a,1,0)]
            ++ [(c,0,1),(c,1,0)])
            [0]

mm1 = updc md0 (perception b (Prp (P 0)) [b,c])
mm1' = upc md0 (perception b (Prp (P 0)) [b,c])

perceived_change ::  Agent ->
                       Prp -> Form -> [Agent] -> FACM State
perceived_change i prp form group ags =
  Acm [0,1,2] ags [(0,(Prp prp,[(prp,form)])),
                   (1,(Neg (Prp prp), [(prp,form)])),
                   (2,(Top,[]))]
      ([(a,s,s)| a <- ags, s <- [0,1,2]]
        ++ [(a,0,1)| a <- ags \\ [i]]
        ++ [(a,1,0)| a <- ags \\ [i]]
        ++ [(a,0,2)| a <- ags \\ group]
        ++ [(a,2,0)| a <- ags \\ group]
        ++ [(a,1,2)| a <- ags \\ group]
        ++ [(a,2,1)| a <- ags \\ group])
      [0]

me1  = updc md0 (perceived_change a (P 0) (Top) [a])
me1' = upc md0 (perceived_change a (P 0) (Top) [a])

me2  = updc md0 (perceived_change a (P 0) (Neg Top) [a])
me2' = upc md0 (perceived_change a (P 0) (Neg Top) [a])

me3  = updc md0 (perceived_change a (P 0) (Neg Top) [a,b])
me3' = upc md0 (perceived_change a (P 0) (Neg Top) [a,b])

me4  = updc md1 (perceived_change a (P 0) (Neg Top) [a,b])
me4' = upc md1 (perceived_change a (P 0) (Neg Top) [a,b])

ppc ::  Agent -> Prp -> Form -> [Agent] -> FACM State
ppc i prp form group ags =
  Acm [0,1,2] ags [(0,(form,[(prp,form)])),
                   (1,(Neg form, [(prp,form)])),
                   (2,(Top,[]))]
      ([(a,s,s)| a <- ags, s <- [0,1,2]]
        ++ [(a,0,1)| a <- ags \\ [i]]
        ++ [(a,1,0)| a <- ags \\ [i]]
        ++ [(a,0,2)| a <- ags \\ group]
        ++ [(a,2,0)| a <- ags \\ group]
        ++ [(a,1,2)| a <- ags \\ group]
        ++ [(a,2,1)| a <- ags \\ group])
      [0]

npc ::  Agent -> Prp -> Form -> [Agent] -> FACM State
npc i prp form group ags =
  Acm [0,1,2] ags [(0,(form,[(prp,form)])),
                   (1,(negation form, [(prp,form)])),
                   (2,(Top,[]))]
      ([(a,s,s)| a <- ags, s <- [0,1,2]]
        ++ [(a,0,1)| a <- ags \\ [i]]
        ++ [(a,1,0)| a <- ags \\ [i]]
        ++ [(a,0,2)| a <- ags \\ group]
        ++ [(a,2,0)| a <- ags \\ group]
        ++ [(a,1,2)| a <- ags \\ group]
        ++ [(a,2,1)| a <- ags \\ group])
      [1]

mpc1  = updc md0 (ppc a (P 0) (Top) [a])
mpc1' = upc md0 (ppc a (P 0) (Top) [a])

mpc2  = updc md0 (npc a (P 0) (Neg Top) [a])
mpc2' = upc md0 (npc a (P 0) (Neg Top) [a])

mpc3  = updc md0 (npc a (P 0) (Neg Top) [a,b])
mpc3' = upc md0 (npc a (P 0) (Neg Top) [a,b])

mpc4  = updc md1 (npc a (P 0) (Neg Top) [a,b])
mpc4' = upc md1 (npc a (P 0) (Neg Top) [a,b])
