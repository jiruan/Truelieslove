module ChangeVocab where

import Data.List
import ModelsVocab hiding (m0)
import ActionVocab hiding (public,preconditions,voc)

type Subst = [(Prp,Form)]

data ACM state = Acm
             [state]
             [Agent]
             [(state,(Form,Subst))]
             [(Agent,state,state)]
             [state]  deriving (Eq,Show)

preconditions :: ACM state -> [Form]
preconditions (Acm _ _ pairs _ _) = map (fst.snd) pairs

voc :: ACM state -> [Prp]
voc acm = (sort.nub.concat) (map getPs (preconditions acm))

type FACM state = [Agent] -> ACM state

prec :: ACM state -> [(state,Form)]
prec (Acm _ _ ps _ _) =
    map (\ (x,(y,_)) -> (x,y)) ps

subst :: ACM state -> [(state,Subst)]
subst (Acm _ _ ps _ _) =
    map (\ (x,(_,y)) -> (x,y)) ps

t2f :: Eq a => [(a,b)] -> a -> b
t2f t = \ x -> maybe undefined id (lookup x t)

sub :: Eq a => ACM a -> a -> Prp -> Form
sub am s p = let
     sb = t2f (subst am) s in
  if elem p (map (\ (x,_) -> x) sb) then
     t2f sb p
   else (Prp p)

val :: Eq a => EpistM a -> a -> [Prp]
val m = t2f (valuation m)

newVal :: (Eq a, Ord a) =>
  EpistM a -> ACM a -> (a,a) -> [Prp]
newVal m am (w,s) = [ p | p <- allprops, subfct p ]
   where
   allprops = (sort.nub)
    ((val  m w) ++
     (map (\ (x,_) -> x) (t2f (subst am) s)))
   subfct p = isTrueAtMayb m w (sub am s p) == Just True

upc :: (Eq state, Ord state) =>
       EpistM state -> FACM state
                    -> EpistM (state,state)

upc m@(Mo worlds ags vc val rel points) facm =
   Mo worlds' ags' vc' val' rel' points'
   where
    acm@(Acm states ags' ps susp as) = facm ags
    worlds' = [ (w,s) | w <- worlds, s <- states,
                 isTrueAtMayb m w (apply (prec acm) s)
                 == Just True ]
    vc' = (sort.nub) (vc ++ voc acm)
    val'    = [ ((w,s),newVal m acm (w,s)) |
                                 (w,s) <- worlds' ]
    rel'    = [ (ag1,(w1,s1),(w2,s2)) |
                 (ag1,w1,w2) <- rel,
                 (ag2,s1,s2) <- susp,
                  ag1 == ag2,
                  elem (w1,s1) worlds',
                  elem (w2,s2) worlds'   ]
    points' = [ (p,a) | p <- points, a <- as,
                       elem (p,a) worlds'    ]

updc ::  (Eq state, Ord state) =>
       EpistM state -> FACM state
                    -> EpistM State
updc m a = bisim (upc m a)

upds :: EpistM State -> [FACM State] -> EpistM State
upds m [] = m
upds m (a:as) = upds (updc m a) as

public :: Form -> FACM State
public form ags = Acm [0] ags [(0,(form,[]))]
                      [(a,0,0)| a <- ags ] [0]

m0 = upc s5example (public p)
m1 = updc s5example (public p)

pChange :: Subst -> FACM State
pChange subst ags = Acm [0] ags [(0,(Top,subst))]
                      [(a,0,0)| a <- ags ] [0]

m2 = upc s5example (pChange [(P 0,q)])
m3 = updc s5example (pChange [(P 0,q)])

groupM :: [Agent] -> Form -> FACM State
groupM gr form agents =
  if sort gr == sort agents
    then public form agents
    else
      (Acm
       [0,1]
        agents
       [(0,(form,[])),(1,(Top,[]))]
        ([ (a,0,0) | a <- agents ]
         ++ [ (a,0,1) | a <- agents \\ gr ]
         ++ [ (a,1,0) | a <- agents \\ gr ]
         ++ [ (a,1,1) | a <- agents          ])
       [0])

e0 = initM [a,b,c] [P 0,Q 0]
m4 = upc e0 (groupM [a,b] (Neg p))
m5 = updc e0 (groupM [a,b] (Neg p))

message :: Agent -> Form -> FACM State
message agent = groupM [agent]

test :: Form -> FACM State
test = groupM []

negation :: Form -> Form
negation (Neg form) = form
negation form       = (Neg form)

info :: [Agent] -> Form -> FACM State
info group form agents =
   Acm
   [0,1]
   agents
   [(0,(form,[])),(1,(negation form,[]))]
   ([ (a,0,0) | a <- agents ]
     ++ [ (a,1,1) | a <- agents ]
     ++ [ (a,0,1) | a <- others ]
     ++ [ (a,1,0) | a <- others ])
   [0,1]
     where others = agents \\ group

m6 = upc e0 (info [a,b] p)
m7 = updc e0 (info [a,b] p)
