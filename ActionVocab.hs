module ActionVocab where

import Data.List
import ModelsVocab
--import Test.QuickCheck

data AM state = Am
             [state]
             [Agent]
             [(state,Form)]
             [(Agent,state,state)]
             [state]  deriving (Eq,Show)

preconditions :: AM state -> [Form]
preconditions (Am _ _ pairs _ _) = map snd pairs

voc :: AM state -> [Prp]
voc am = (sort.nub.concat) (map getPs (preconditions am))

type FAM state = [Agent] -> AM state

up :: (Eq state, Ord state) =>
       EpistM state -> FAM state
                    -> EpistM (state,state)

up m@(Mo worlds ags voc val rel points) fam =
   Mo worlds' ags' voc val' rel' points'
   where
   Am states ags' pre susp actuals = fam ags
   worlds' = [ (w,s) | w <- worlds, s <- states,
                    isTrueAtMayb m w (apply pre s) ==
                    Just True]
   val'    = [ ((w,s),props) | (w,props) <- val,
                                s        <- states,
                                elem (w,s) worlds']
   rel'    = [ (ag1,(w1,s1),(w2,s2)) |
                 (ag1,w1,w2) <- rel,
                 (ag2,s1,s2) <- susp,
                  ag1 == ag2,
                  elem (w1,s1) worlds',
                  elem (w2,s2) worlds'   ]
   points' = [ (p,a) | p <- points, a <- actuals,
                       elem (p,a) worlds'         ]

upd :: (Eq state, Ord state) =>
       EpistM state -> FAM state
                    -> EpistM State
upd m a = bisim (up m a)

public :: Form -> FAM State
public form ags = Am [0] ags [(0,form)]
                      [(a,0,0)| a <- ags ] [0]

composMod :: (Eq a, Ord a) =>
       EpistM a -> EpistM a
                    -> EpistM (a,a)

composMod m1@(Mo worlds  agents voc  val1 rel1 points)
          m2@(Mo worlds'  agents' voc' val2 rel2 points') =
             (Mo compstat agents compvoc  compval  comprel
                 compoints) where

 compstat = [(x,y) | x <- worlds, y <- worlds', intersect
            (valStat x m1) (vcbSet m2)== intersect
            (valStat y m2) (vcbSet m1)]

 comprel = [(i,(x,y),(r,s))| (i,x,r) <- rel1,
            (j,y,s)  <- rel2, i==j]

 compval = [((x,y), nub ((++) (vcbSet m1) (vcbSet m2)))|
            x <- worlds, y <- worlds']
 compvoc = (sort.nub) ((++) voc voc')
 compoints = [(x,y) | x <- points, y <- points']

compos :: (Eq a, Ord a) =>
       EpistM a -> EpistM a -> EpistM State

compos m1@(Mo worlds  agents voc  val1 rel1 points)
       m2@(Mo worlds'  agents' voc' val2 rel2 points') =
            bisim (composMod m1 m2)
