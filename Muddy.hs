module Muddy where 

import Data.List
import ModelsVocab hiding (m0)
import ActionVocab hiding (upd,public,preconditions,voc)
import ChangeVocab
import ChangePerception

ma, mb, mc, md :: Form
ma = Prp (P 1) -- this represents Alice is muddy 
mb = Prp (P 2) -- this represents Bob is muddy 
mc = Prp (P 3) -- this represents Carol is muddy 
md = Prp (P 4) -- this represents Dave is muddy 

bcd_dirty = Conj [Neg ma, mb, mc, md]

awareness = [info [b,c,d] ma, 
             info [a,c,d] mb,
             info [a,b,d] mc, 
             info [a,b,c] md ]

aKn =  Disj [K a ma, K a (Neg ma)]
bKn =  Disj [K b mb, K b (Neg mb)]
cKn =  Disj [K c mc, K c (Neg mc)]
dKn =  Disj [K d md, K d (Neg md)]

mu0 = upd (initM [a,b,c,d] [P 1, P 2, P 3, P 4]) (test bcd_dirty)

mu1 = upds mu0 awareness 

mu2 = upd  mu1 (public (Disj [ma, mb, mc, md])) 

mu3 = upd  mu2 (public (Conj[Neg aKn, Neg bKn, Neg cKn, Neg dKn]))

mu4 = upd  mu3 (public (Conj[Neg aKn, Neg bKn, Neg cKn, Neg dKn]))

mu5 = upds mu4 [public (Conj[bKn, cKn, dKn])]

